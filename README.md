# delightful matrix [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/master/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of delightful Matrix resources, implementations and clients.

<!-- Everyone is invited to contribute. To do so, please read guidelines at: https://codeberg.org/teaserbot-labs/delightful -->

## Table of contents

- [General resourcess](#general-resources)
- [Server implementations](#server-implementations)
- [Clients](#clients)
- [Client SDKs](#client-sdks)
- [Projects based on Matrix](#projects-based-on-matrix)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

Emoji for each entry provide additional information on project status:
- 👻 == inactive for over a year, or officially abandoned

## General resources

- [Matrix Spec](https://spec.matrix.org/latest/): the official spec that defines the communication protocols and allows all servers and apps to communicate with each other. `Apache-2.0`

## Server implementations

- [Conduit](https://gitlab.com/famedly/conduit) ([site](https://conduit.rs)): A lightweight open-source server implementation of the Matrix Specification with a focus on easy setup and low system requirements. `Apache-2.0, Rust`
server developed independently by the community. `BSD, C++`
- [Dendrite](https://github.com/matrix-org/dendrite) ([site](https://matrix.org/docs/projects/server/dendrite)): A second-generation Matrix homeserver written in Go. `Apache-2.0, Go`
- [Synapse](https://github.com/matrix-org/synapse) ([site](https://matrix.org/docs/projects/server/synapse)): Matrix homeserver written in Python 3/Twisted. `Apache-2.0, Python`
- 👻 [Construct](https://github.com/matrix-construct/construct): A performance-oriented homeserver with minimal dependencies, the first actively federating Matrix
- 👻 [Ligase](https://github.com/finogeeks/Ligase): A Cloud-native Matrix home server written in Golang. `AGPL-3.0, Go`
- 👻 [Maelstrom](https://github.com/maelstrom-rs/maelstrom): A high-performance Matrix Home-Server written in Rust designed to be scalable, light on resources, and have a pluggable storage engine. `Apache-2.0 or MIT, Rust`

## Clients

- [[chat]](https://git.cybre.town/adb/matrix-chat) ([app](https://chat.adb.sh)): A simple matrix webapp for mobile and desktop. `MPL-2.0, JavaScript`
- [Cinny](https://github.com/ajbura/cinny) ([site](https://cinny.in)): Yet another matrix client (web). `MIT, JavaScript`
- [Ditto Chat](https://gitlab.com/ditto-chat/ditto) ([site](https://www.dittochat.org)): An attractive Matrix client built in React Native (Android, iOS, web). `GPL-3.0-or-later, JavaScript`
- [Element](https://github.com/vector-im) ([site](https://element.io)): A glossy web client with an emphasis on performance and usability (Android, iOS, desktop, web). `Apache 2.0, JavaScript/Kotlin/Objective-C`
- [FluffyChat](https://gitlab.com/famedly/fluffychat) ([site](https://fluffychat.im)): Cute instant messaging app for all platforms (Android, iOS, desktop, web). `AGPL-3.0-only, Dart`
- [Fractal](https://gitlab.gnome.org/GNOME/fractal) ([site](https://wiki.gnome.org/Apps/Fractal)): A Matrix messaging app for GNOME written in Rust (Linux). `GPL-3.0-only, Rust`
- [gomuks](https://github.com/tulir/gomuks) ([site](https://matrix.org/docs/projects/client/gomuks)): A terminal based Matrix client written in Go. `AGPL-3.0-or-later, Go`
- [Hydrogen](https://github.com/vector-im/hydrogen-web) ([app](https://hydrogen.element.io)): Lightweight matrix client with legacy and mobile browser support (web). `Apache-2.0, TypeScript`
- [kazv](https://lily-is.land/kazv/kazv): A convergent qml/kirigami client based on libkazv (Linux). `AGPL-3.0-or-later, C++`
- [matrix-commander](https://github.com/8go/matrix-commander): Simple but convenient CLI-based Matrix client app for sending, receiving, creating rooms, inviting, verifying, and so much more. `GPL-3.0-or-later, Python`
- [matrix-static](https://github.com/matrix-org/matrix-static): A static golang generated preview of public world readable Matrix rooms. `Apache-2.0, Go`
- [NeoChat](https://invent.kde.org/network/neochat): A client for Matrix (desktop, mobile). `GPL-3.0-only, C++`
- [Nheko](https://github.com/Nheko-Reborn/nheko) ([site](https://nheko-reborn.github.io)): A native desktop app for Matrix that feels more like a mainstream chat app (Linux, macOS). `GPL-3.0-or-later, C++`
- [Nio](https://github.com/niochat/nio) ([site](https://nio.chat)): An upcoming Matrix client for iOS built with SwiftUI. `MPL-2.0, Swift`
- [Quaternion](https://github.com/quotient-im/Quaternion): A Qt5-based IM client for Matrix (desktop). `GPL-3.0-only, C++ `
- [QuickMedia](https://git.dec05eba.com/QuickMedia/about): A native client for web services including Matrix. `GPL-3.0-or-later, C++`
- [SchildiChat](https://github.com/SchildiChat) ([site](https://schildi.chat)): A Matrix client based on Element with a more traditional instant messaging experience (Android, desktop, web). `Apache-2.0, JavaScript/Kotlin`
- [Syphon](https://github.com/syphon-org/syphon) ([site](https://syphon.org)): A not for profit, open source matrix client with a focus on privacy and ease of use (mobile, desktop). `AGPL-3.0-or-later, Dart`
- 👻 [AgentSmith](https://github.com/nilsding/AgentSmith): An IRC server that is actually a Matrix client. Use your favourite IRC client to communicate with the Matrix. `MIT, Crystal`
- 👻 [Koma](https://github.com/koma-im/continuum-desktop): A pure Kotlin Matrix client (Linux, macOS). `GPL-3.0-only, Kotlin`
- 👻 [matrix-client.el](https://github.com/alphapapa/matrix-client.el): A Matrix client for Emacs. `GPL-3.0-only, Lisp`
- 👻 [matrix-ircd](https://github.com/matrix-org/matrix-ircd): An IRCd implementation backed by Matrix. `Apache-2.0, Rust`
- 👻 [Mirage](https://github.com/mirukana/mirage): A fancy, customizable, keyboard-operable Qt/QML & Python Matrix chat client (desktop). `LGPL-3.0-or-later, Python`
- 👻 [Scylla](https://github.com/DanilaFe/Scylla) ([app](https://scylla.danilafe.com)): An Elm-based front-end for Matrix (web) `MIT, Elm`
- 👻 [Rambox](https://github.com/ramboxapp/community-edition) ([site](https://rambox.app)): Cross platform messaging and emailing app that combines common web applications into one. `GPL-3.0-only, JavaScript`

## Client SDKs

### TypeScript & JavaScript
- [matrix-appservice-bridge](https://github.com/matrix-org/matrix-appservice-bridge): Bridging infrastructure for Application Services. `Apache-2.0, TypeScript`
- [matrix-appservice-node](https://github.com/matrix-org/matrix-appservice-node): Matrix Application Service framework in Node.js. `Apache-2.0, TypeScript`
- [matrix-bot-sdk](https://github.com/turt2live/matrix-bot-sdk): TypeScript/JavaScript SDK for Matrix bots. `MIT, TypeScript`
- [matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk): A Matrix Client-Server SDK for JavaScript. `Apache-2.0, TypeScript`
- [matrix-react-sdk](https://github.com/matrix-org/matrix-react-sdk): A Matrix SDK for React Javascript. `Apache-2.0, TypeScript`
- [smallbot-matrix](https://github.com/enimatek-nl/small-bot-matrix): Small Matrix Little Bot for Deno. `MIT, TypeScript`
- 👻 [botkit-matrix](https://github.com/frankgerhardt/botkit-matrix): A Botkit connector for Matrix. `Apache-2.0, JavaScript`

### Python
- [mautrix-python](https://github.com/mautrix/python): A Python 3 asyncio Matrix framework. `MPL-2.0, Python`
- [simple-matrix-bot-lib](https://github.com/KrazyKirby99999/simple-matrix-bot-lib): An easy to use bot library for the Matrix ecosystem written in Python. `MIT, Python`
- [µtrix](https://edugit.org/Teckids/hacknfun/libs/mytrix): A Matrix client library for MicroPython. `Apache-2.0, Python`
- 👻 [matrix-python-sdk](https://github.com/matrix-org/matrix-python-sdk): A Matrix Client-Server SDK for Python 2 and 3. `Apache-2.0, Python`

### Kotlin
- [dial-phone](https://github.com/mtorials/dial-phone): A Matrix client-server SDK for JVM written in Kotlin. `Apache-2.0, Kotlin`
- [matrix-kt](https://github.com/Dominaezzz/matrix-kt): Kotlin multiplatform libraries for Matrix. `Apache-2.0, Kotlin`
- [Trixnity](https://gitlab.com/benkuly/trixnity): A Multiplatform Kotlin SDK for Matrix. `AGPL-3.0-or-later, Kotlin`

### Rust
- [matrix-rust-sdk](https://github.com/matrix-org/matrix-rust-sdk): Matrix Client-Server SDK for Rust. `Apache-2.0, Rust`
- [ruma](https://github.com/ruma/ruma) ([site](https://www.ruma.io)): A set of Rust crates for interacting with the Matrix chat network. `MIT, Rust`

### C++
- [libkazb](https://lily-is.land/kazv/libkazv): A matrix client SDK built upon lager and the value-oriented design it enables. `AGPL-3.0-or-later, C++`
- [libQuotient](https://github.com/quotient-im/libQuotient): A Qt5 library to write cross-platform clients for Matrix. `LGPL-2.1, C++`

### C#
- [Matrix .NET SDK](https://github.com/baking-bad/matrix-dotnet-sdk): A .NET Standard 2.0 library compatible with the Matrix protocol. `MIT, C#`
- 👻 [MatrixAPI](https://github.com/VRocker/MatrixAPI): A Matrix library for C# UWP. `Apache-2.0, C#`

### Go
- [mautrix-go](https://github.com/mautrix/go) ([site](https://maunium.net/go/mautrix/)): A Golang Matrix framework. `MPL-2.0, Go`
- 👻 [gomatrix](https://github.com/matrix-org/gomatrix): A Golang Matrix client. `Apache-2.0, Go`

### Other languages
- [dart-matrix-sdk](https://gitlab.com/famedly/company/frontend/famedlysdk): A Matrix SDK written in pure Dart. `AGPL-3.0, Dart`
- [Matrix::Client](https://github.com/matiaslina/Matrix-Client): A Raku library for Matrix. `Artistic-2.0, Raku`
- [Matrix-ClientServer-API-java](https://github.com/JojiiOfficial/Matrix-ClientServer-API-java): A small and simple java API. `GPL-3.0, Java`
- [matrix-ios-sdk](https://github.com/matrix-org/matrix-ios-sdk): A Matrix SDK for iOS. `Apache-2.0, Objective-C`
- [matrix-nio](https://github.com/poljar/matrix-nio) ([site](https://matrix-nio.readthedocs.io/en/latest/)): A Python Matrix client library, designed according to sans I/O principles. `ISC, Python`
- [ruby-matrix-sdk](https://github.com/ananace/ruby-matrix-sdk): A Ruby SDK for the Matrix communication protocol. `MIT, Ruby`
- 👻 [haxe-matrix-im](https://notabug.org/Tamaimo/haxe-matrix-im): Implementation of client-server matrix API in haxe using HaxeHttpClient. `AGPL-3.0-only, Haxe`

## Projects based on Matrix

- [Cactus Comments](https://gitlab.com/cactus-comments) ([site](https://cactus.chat)): A federated comment system for the web, based on the Matrix protocol. `GPL-3.0, Python`

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/yarmo/delightful-matrix/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@yarmo`](https://codeberg.org/yarmo)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)
